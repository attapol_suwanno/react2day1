import React from "react";

class Userpage extends React.Component {
  constructor() {
    super();
    this.state = {
      users: []
    };
  }
  componentDidMount() {
    this.fetchUser();
  }

  fetchUser() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({ users: data });
      })
      .catch(error => console.log(error));
  }
  render() {
    var { users } = this.state;
    // สามารถสร้างคอมม่าไปได้เลยไม่ต้องสร้างสองตัว
        // เหมือนกัน  
    // var users=this.state.users;
    console.log(users);

    return (
      <div>
        <div>User Page</div>
        <div>
          {users.map((item, index) => {
            return (
              <div key={index}>
                <p>{item.id}</p>
                <p>{item.name}</p>
                <p>{item.username}</p>
                <hr />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
export default Userpage;
