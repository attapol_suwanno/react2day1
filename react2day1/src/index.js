import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {BrowserRouter,Route} from 'react-router-dom'
import Userpage from './page/Userpage';
import Homepage from './page/Homepage';
import Header from './component/Header'


const MainRouting =
<BrowserRouter>
<Route path="/" component={Header}></Route>
<Route path='/users' component={Userpage}></Route>
<Route path="/home" component={Homepage}></Route>
</BrowserRouter>

ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
